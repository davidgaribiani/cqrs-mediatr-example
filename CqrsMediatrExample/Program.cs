using CqrsMediatrExample.Behaviors;
using MediatR;

namespace CqrsMediatrExample
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddMediatR(typeof(Program));
            builder.Services.AddSingleton<FakeDataStore>();
            builder.Services.AddSingleton(typeof(IPipelineBehavior<,>), typeof(LogginBehavior<,>));

            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}